# Minutes of Meeting

* **Tasks** - has an Owner and a deadline
* **Decisions** (D)
* **Information** (I) - everything else of interest.
 
### Date : 19/01/2021
### Time : 19:00
### Topic: First Client Meet
***Organiser:*** Paryul Jain

**Absentees:** None\
**Attendees:** Miss Priya,Miss Sravanthi, Miss Balamma, Mr.Raj, Pranjal Jain, Stella Sravanthi, Shreyansh Verma, Aryan Dubey

Type | Description | Owner | Deadline
---- | ---- | ---- | ----
T | Convert the website to Javascript | Team 30 | April
T | To get familarised with the project and all its requirements | Client | 27/01/21
D | Use only Javasript(ES6 syntax) from back to front | - | -
D | Avoid inline styling completely instead use Sass | - | -
I | Discussed the motive of the project to migrate the website from PHP to Javascript based | - | -

**Next Meet:** 27/01/21\
**End Time:** 20:00